(use-modules (ice-9 textual-ports))

;; Using the recommended Textual I/O described in:
;; https://www.gnu.org/software/guile/manual/html_node/Textual-I_002fO.html#Textual-I_002fO

(define (read-line)
  (define port (current-input-port))
  (get-line port))


(define (write-string string)
  (define port (current-output-port))
  (put-string port string))
