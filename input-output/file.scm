(use-modules (ice-9 textual-ports)  ; for textual reading and writing procedures
             (ice-9 binary-ports)  ; not sure if needed
             (ice-9 rdelim)  ; for `eof-object?`
             (ice-9 optargs)  ; for keyword arguments
             (srfi srfi-1))  ; not sure if needed


(define* (get-string-from-file file-path #:key (encoding "UTF-8"))
  (call-with-input-file file-path
    (λ (port)
      (set-port-encoding! port encoding)
      (get-string-all port))))


(define* (get-lines-from-file file-path #:key (encoding "UTF-8"))
  ;; another common encoding is: "ISO-8859-1"
  ;; see http://www.iana.org/assignments/character-sets for more
  (define (get-lines-from-port port)
    (let ([line (get-line port)])
      (cond [(eof-object? line) '()]
            [else
             (cons line
                   (get-lines-from-port port))])))
  (call-with-input-file file-path
    (lambda (port)
      (set-port-encoding! port encoding)
      (get-lines-from-port port))))


(define* (get-lines-from-file-using-splitting file-path #:key (encoding "UTF-8"))
  (string-split (get-string-from-file file-path #:encoding encoding)
                ;; You could use simple character here, but I am using
                ;; lambda to show that it can be used as well.
                (lambda (char) (char=? char #\newline))))


(define* (put-lines-to-file file-path lines #:key (encoding "UTF-8") (mode 'replace))
  (call-with-output-file file-path
    (λ (port)
      (set-port-encoding! port encoding)
      (put-string port
                  (cond [(eq? mode 'append)
                         (string-append (get-string-from-file
                                         file-path
                                         #:encoding encoding)
                                        "\n"
                                        (string-join lines "\n"))]
                        [(equal? mode 'replace) (string-join lines "\n")]
                        [else (string-join lines "\n")])))))


(define* (put-string-to-file file-path string #:key (encoding "UTF-8") (mode 'replace))
  (call-with-output-file file-path
    (λ (port)
      (set-port-encoding! port encoding)
      (put-string port
                  (cond [(eq? mode 'append)
                         (string-append (get-string-from-file
                                         file-path
                                         #:encoding encoding)
                                        "\n"
                                        string)]
                        [(equal? mode 'replace) string]
                        [else string])))))
