(use-modules (ncurses curses))

;; Get the screen.
(define stdscr (initscr))
;; Add a string to the screen.
(addstr stdscr "Hello World!!!")
;; Refresh the screen. -- Why do we do this? String is shown without
;; refreshing the screen. Maybe this is an oddity only on my system.
(refresh stdscr)
;; Wait for a character to be entered.
(getch stdscr)
;; Then end the window, effectively ending the program.
(endwin)
