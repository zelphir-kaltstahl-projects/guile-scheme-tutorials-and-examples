(define chessboard-letter->column-table
  (alist->hash-table
   '((a . 0) (b . 1) (c . 2) (d . 3) (e . 4) (f . 5) (g . 6) (h . 7))))

(hash-ref chessboard-letter->column-table
          'a
          #f)
