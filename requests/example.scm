(use-modules (web client)
             #;(ice-9 receive))

(receive (response response-text)
    (http-get "http://duckduckgo.com"
              #:port (open-socket-for-uri "http://duckduckgo.com")
              #:version '(1 . 1)
              #:keep-alive? #f
              #:headers '()
              #:decode-body? #t
              #:streaming? #f)
  (display response)
  (display response-text))


(call-with-values
    (lambda ()
      (http-get "https://duckduckgo.com"
                #:port (open-socket-for-uri "https://duckduckgo.com")
                #:version '(1 . 1)
                #:keep-alive? #f
                #:headers '()
                #:decode-body? #t
                #:streaming? #f))
  (lambda (response response-text)
    (display response)
    (display response-text)
    (display (response-headers response))))


#|

HEADERS:

'((server . nginx)
  (date . #<date nanosecond: 0 second: 32 minute: 30 hour: 13 day: 2 month: 6 year: 2019 zone-offset: 0>)
  (content-type text/html (charset . UTF-8))
  (content-length . 5418)
  (connection close)
  (vary accept-encoding)
  (etag 5cf2d4ab-152a . #t)
  (strict-transport-security . max-age=31536000)
  (x-frame-options . SAMEORIGIN)
  (content-security-policy . default-src https: blob: data: 'unsafe-inline' 'unsafe-eval'; frame-ancestors 'self')
  (x-xss-protection . 1;mode=block)
  (x-content-type-options . nosniff)
  (referrer-policy . origin)
  (expect-ct . max-age=0)
  (expires . #<date nanosecond: 0 second: 31 minute: 30 hour: 13 day: 2 month: 6 year: 2019 zone-offset: 0>)
  (cache-control no-cache)
  (accept-ranges bytes))

|#


#|

If you already have a port open, pass it as port. Otherwise, a
connection will be opened to the server corresponding to uri. Any
extra headers in the alist headers will be added to the request.

If body is not #f, a message body will also be sent with the HTTP
request. If body is a string, it is encoded according to the
content-type in headers, defaulting to UTF-8. Otherwise body should be
a bytevector, or #f for no body. Although a message body may be sent
with any request, usually only POST and PUT requests have bodies.

If decode-body? is true, as is the default, the body of the response
will be decoded to string, if it is a textual content-type. Otherwise
it will be returned as a bytevector.

However, if streaming? is true, instead of eagerly reading the
response body from the server, this function only reads off the
headers. The response body will be returned as a port on which the
data may be read.

Unless keep-alive? is true, the port will be closed after the full
response body has been read.

Returns two values: the response read from the server, and the
response body as a string, bytevector, #f value, or as a port (if
streaming? is true).

|#
