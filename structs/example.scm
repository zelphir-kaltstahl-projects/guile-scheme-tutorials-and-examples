;; SRFI 9: record types
(use-modules
 ;; structs
 (srfi srfi-9)
 ;; for functional structs (not part of srfi-9 directly)
 (srfi srfi-9 gnu))

;; ==============
;; NORMAL STRUCTS
;; ==============
(define-record-type <bitboard>
  ;; define constructor
  (make-bitboard bits height width kind)
  ;; define predicate
  bitboard?
  ;; define accessors
  (bits bitboard-bits)
  (height bitboard-height)
  (width bitboard-width)
  (kind bitboard-kind))

(set-record-type-printer!
 <bitboard>
 (λ (record port)
   (display
    (simple-format port
                   "Bitbard: <bits: ~a, height: ~a, width: ~a, kind: ~s>\n"
                   (bitboard-bits record)
                   (bitboard-height record)
                   (bitboard-width record)
                   (bitboard-kind record)))))

;; ==================
;; FUNCTIONAL STRUCTS
;; ==================
;; "[...] the define-immutable-record-type works like define-record-type, except that fields are immutable and setters are defined as functional setters."
(define-immutable-record-type <bitboard>
  ;; define constructor
  (make-bitboard bits height width kind)
  ;; define predicate
  bitboard?
  ;; define accessors and functional setters
  (bits bitboard-bits set-bitboard-bits)
  (height bitboard-height set-bitboard-height)
  (width bitboard-width set-bitboard-width)
  (kind bitboard-kind set-bitboard-kind))

;; =====
;; USAGE
;; =====
(let ([height 9]
      [width 9])
  (let ([num-bits (* height width)])
    (display (make-bitboard (make-bitvector num-bits 'P) 9 9 'shogi))))
