;; IMPORTANT!
;; The module must be named exactly like the name of the file it is in.
(define-module (hello-world-lib)
  #:export (hello
            world
            exclamation-mark))

;; IMPORTANT!
;; use-modules comes AFTER the module definition!
(use-modules (srfi srfi-9)
             (srfi srfi-9 gnu))

(define hello
  (λ ()
    (display "hello")))

(define world
  (λ ()
    (display "world")))

(define exclamation-mark
  (λ ()
    (display "!")))
