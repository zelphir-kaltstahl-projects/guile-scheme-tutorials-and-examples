;; IMPORTANT!
;; If the module is defined in a subdirectory, the subdirectory must be
;; mentioned as in this example and the names must be equal to the subdirectory
;; name and the file name of the file, which defines the module.
(define-module (subdir mention)
  #:export (mention))

(define mention
  (λ (name)
    (display (string-append name "bert"))))
