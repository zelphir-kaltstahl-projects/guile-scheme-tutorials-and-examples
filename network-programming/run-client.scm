(use-modules ((tcp-client) #:prefix client:)
             (ice-9 textual-ports)
             (json))


(define (client-json-display-message-handler in-out-sock scm-native)
  (display (simple-format #f "RECEIVED JSON: ~s, which is: ~s\n"
                          scm-native
                          (scm->json-string scm-native))))


(define client-json-display-protocol
  (client:make-client-protocol
   #:port-reader json->scm
   #:message-handler client-json-display-message-handler
   #:eof-handler close))


(define client-sock
  (client:run-client 12345
                     #:protocol client-json-display-protocol))


(scm->json '((a . 1)) client-sock)


;; To run this you need to add the containing directory to the load path.
;; (add-to-load-path "YOUR DIRECTORY HERE")
