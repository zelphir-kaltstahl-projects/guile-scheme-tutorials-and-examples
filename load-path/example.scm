(add-to-load-path (dirname (current-filename)))

(display
 (simple-format #f
                "current directory: ~a\n"
                (dirname (current-filename))))

(display
 (simple-format #f
                "%load-path: ~a\n"
                %load-path))
