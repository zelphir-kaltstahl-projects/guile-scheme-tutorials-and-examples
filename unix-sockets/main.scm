(use-modules (ice-9 iconv))

(define* (connect-to-unix-socket #:key (socket-path "/var/run/docker.sock"))
  (let ([sock-addr (make-socket-address AF_UNIX socket-path)]
        [sock (socket PF_UNIX SOCK_STREAM 0)])
    ;; socket options:
    ;; https://www.gnu.org/software/libc/manual/html_node/Socket_002dLevel-Options.html
    (setsockopt sock SOL_SOCKET SO_REUSEADDR 1)
    ;; usage of connect:
    ;; https://www.gnu.org/software/guile/manual/html_node/Network-Sockets-and-Communication.html#Network-Sockets-and-Communication
    ;; server side would use `bind`, `accept` and `listen`.
    ;; client side uses `connect` and `close`.
    (connect sock sock-addr)
    sock))

;; Syntax for a request is:
;; PROTOCOL://unix:SOCKET_PATH:ENDPOINT_PATH

;; https://www.gnu.org/software/guile/docs/docs-2.0/guile-ref/Network-Sockets-and-Communication.html
;; (send sock message)
