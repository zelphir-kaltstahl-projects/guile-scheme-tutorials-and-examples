;; Here is some example macro for defining tests:

;; (define-syntax test-check
;;   (syntax-rules ()
;;     ((_ title tested-expression expected-result)
;;      (begin
;;        (format #t "* Checking ~s\n" title)
;;        (let* ((expected expected-result)
;;               (produced tested-expression))
;;          (if (not (equal? expected produced))
;;              (begin (format #t "Expected: ~s\n" expected)
;;                     (format #t "Computed: ~s\n" produced))))))))

;; It can be used as follows:

;; (test-check "TITLE OF TEST"
;;             expression
;;             expected-result)

;; However, what other unit-testing facilities does Guile offer?




;; =======
;; SRFI-64
;; =======
;; The official Guile manual links to SRFI 64.

(use-modules (srfi srfi-64))

;; Example from the docs:
;; Initialize and give a name to a simple testsuite.
(test-begin "vec-test")
(test-group
 "vec-test"

 (define v (make-vector 5 99))
 ;; Require that an expression evaluate to true.
 (test-assert (vector? v))
 ;; Test that an expression is eqv? to some other expression.
 (test-eqv 99 (vector-ref v 2))
 (vector-set! v 2 7)
 (test-eqv 7 (vector-ref v 2)))
;; Finish the testsuite, and report results.
(test-end "vec-test")
