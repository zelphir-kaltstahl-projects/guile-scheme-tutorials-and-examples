;; =================================
;; Creating arrays using expressions
;; =================================

;; define a 2D array ...
#2()

;; ... of unsigned integers of 8 bit length ...

#2u8()

;; ... of specified lengths, needing intial values ...

#2u8:4:4((0 0 0 0)
         (0 0 0 0)
         (0 0 0 0)
         (0 0 0 0))

;; ... of specified indice starts

#2u8@2:4@4:4((0 0 0 0)
             (0 0 0 0)
             (0 0 0 0)
             (0 0 0 0))

;; ================================
;; Creating arrays using procedures
;; ================================

;; specifying the number of elements inside the array with an initial value
;; (make-array fill bounds ...)
(make-array 'ho 2 3)  ; --> #2((ho ho ho) (ho ho ho))

;; specifying ranges of the dimensions
;; '(0 1) will contain 2 items.
;; '(0 2) is the range from 0 to 2 inclusive, and thus is 3 items.
(make-array 'ho '(0 1) '(0 2))  ; #2((ho ho ho) (ho ho ho))

;; making a typed array of type u8 filled with 255 and size 2x2:
(make-typed-array 'u8 255 2 2)
;; but not this ...
(make-typed-array 'u8 256 2 2)
;;  --> ERROR: In procedure make-srfi-4-vector:
;;      In procedure bytevector-u8-set!: Value out of range: 256

;; ==========
;; Predicates
;; ==========
(array? #2u8:4:4((0 0 0 0)
                 (0 0 0 0)
                 (0 0 0 0)
                 (0 0 0 0)))  ; #t
(array? '())  ; #f

(typed-array? #2u8:4:4((0 0 0 0)
                       (0 0 0 0)
                       (0 0 0 0)
                       (0 0 0 0))
              'u8)  ; #t
(typed-array? #2:4:4((0 0 0 0)
                     (0 0 0 0)
                     (0 0 0 0)
                     (0 0 0 0))
              'u8)  ; #f


;; ===========
;; Referencing
;; ===========
(array-ref
 #2:4:4((0 1 2 3)
        (4 5 6 7)
        (8 9 10 11)
        (12 13 14 15))
 3 3)







;; continue: https://www.gnu.org/software/guile/manual/html_node/Array-Procedures.html#Array-Procedures
