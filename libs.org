* Tools / Libraries

** Web Development

*** JWT

- guile-jwt

*** Web Server

- GNU Artanis -- https://www.gnu.org/software/artanis/, https://gitlab.com/NalaGinrut/artanis, https://web-artanis.com/
- SPH web app? -- https://github.com/sph-mn/sph-web-app, http://sph.mn/c/view/mu
- Guile Webserver? -- [[https://www.gnu.org/software/guile/manual/html_node/Web-Server.html#Web-Server][Guile Webserver]] -- [[https://wingolog.org/archives/2012/03/08/an-in-depth-look-at-the-performance-of-guiles-web-server][Andy Wingo about performance of guile's web server]] -- Intro
  - generic interface for web server implementations: https://www.gnu.org/software/guile/manual/html_node/Web-Server.html
  - examples for usage to create a web app: https://www.gnu.org/software/guile/manual/html_node/Web-Examples.html#Web-Examples
*** Template Engine

- just use SXML?

*** Static Blogging

- [[https://github.com/guildhall/guile-haunt][guile-haunt]]
- [[https://wingolog.org/projects/tekuti/][tekuti]] -- by Andy Wingo?

*** Formats

- JSON: [[https://github.com/aconchillo/guile-json][guile-json]]
- CSV: https://gitlab.com/NalaGinrut/guile-csv
- If no input from user: might be able to read/write s-expr

*** Server-side = Client-side

- serve the client biwascheme (but no TCO ... except in Safari)

*** Databases

- GNU Artanis has suport for databases – How does it access databases? Maybe another library?

** GUI

- Bindings for https://cairographics.org/: guile-cairo


** Game

- [[https://dthompson.us/projects/chickadee.html][Chickadee]] -- by dthompson (David Thompson <davet@gnu.org>)

** Image Processing

- [[https://www.gnu.org/software/guile-cv/][Guile-CV]]
