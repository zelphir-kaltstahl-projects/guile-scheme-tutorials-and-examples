;; (define (factorial n)
;;   (cond
;;    [(> n 1) (* n (factorial (- n 1)))]
;;    [else 1]))

;; (display "FACTORIAL:") (newline)
;; (display (factorial 5000)) (newline)

;; (define (fibonacci n)
;;   (cond [(> n 2)
;;          (+ (fibonacci (- n 1))
;;             (fibonacci (- n 2)))]
;;         [else 1]))

;; (display "FIBONACCI:") (newline)
;; (display (fibonacci 1300)) (newline)

(define (factorial-iter n)
  (define (iter n product)
    (cond [(> n 1) (iter (- n 1) (* product n))]
          [else product]))
  (iter n 1))

(display "FACTORIAL:") (newline)
(display (factorial-iter 5000)) (newline)
