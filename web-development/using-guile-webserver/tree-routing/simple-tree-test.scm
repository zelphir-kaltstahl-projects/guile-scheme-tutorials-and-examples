(add-to-load-path (dirname (current-filename)))

(use-modules (srfi srfi-64)
             (simple-tree))


(test-begin "simple-tree")

(test-group
 "get-node-by-path-test"
 (let ([atree
        (list->tree
         '(a b
             ((c c-val
                 ((h h-val
                     ((i i-val ())
                      (k k-val ())))))
              (d d-val ())
              (e e-val
                 ((f f-val ()))))))])
   (test-equal (get-node-by-path atree '(a c h i k) #t #:use-longest-prefix #t)
     (make-node 'i 'i-val '()))
   (test-equal (get-node-by-path atree '(a c h i k) 'default-value)
     (make-node 'default-result 'default-value '()))
   (test-equal (get-node-by-path atree '(a c h i k))
     (make-node 'default-result #f '()))))

(test-end "simple-tree")
