(use-modules (web server))
(use-modules (web request)
             (web response)
             (web uri))


(define (request-path-components request)
  ;; just for showing what the functions do
  (display (simple-format #f "(request-uri request): ~a\n"
                          (request-uri request)))
  (display (simple-format #f "(uri-path ...): ~a\n"
                          (uri-path (request-uri request))))
  (display (simple-format #f "(split-and-decode-uri-path ...): ~a\n"
                          (split-and-decode-uri-path (uri-path (request-uri request)))))
  ;; actual logic
  ;; split the string that represents the uri and decode any url-endoced things
  (split-and-decode-uri-path
   ;; get the uri path as a string from the request struct
   (uri-path
    ;; get the request struct
    (request-uri request))))

(define (not-found request)
  (values (build-response #:code 404)
          (string-append "Resource not found: "
                         (uri->string (request-uri request)))))

(define (hello-hacker-handler request body)
  (if (equal? (request-path-components request) '("hacker"))
      ;; answer in plain text "Hello hacker!"
      (values '((content-type . (text/plain)))
              "Hello hacker!")
      (not-found request)))

(run-server hello-hacker-handler)
