(use-modules (web server))

;; 1. implement a web handler
(define (hello-world-handler request request-body)
  (values
   ;; headers first
   '((content-type . (text/plain)))
   ;; then the response body
   "Hello World!"))

;; 2. Ready to go, let's start the web server
;; by default the server runs on port 8080
(run-server hello-world-handler)
