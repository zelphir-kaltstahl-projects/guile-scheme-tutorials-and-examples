(use-modules (ice-9 threads))

(define (sleep-random-time max)
  (usleep (random max)))

(define displayln
  (λ (sth)
    (display (simple-format #f "~a\n" sth))))

(define (make-threads thread-count max-sleep)
  (define (iter counter)
    (cond [(> counter 0)
           (cons (call-with-new-thread
                  (λ ()
                    (sleep-random-time max-sleep)
                    (displayln (simple-format #f "thread ~a done" (- thread-count counter)))))
                 (iter (- counter 1)))]
          [else '()]))
  (iter thread-count))

(define (main)
  (define max-sleep 500)
  (define start-time (current-time))
  (displayln (all-threads))
  (displayln (current-thread))

  (let ([threads (make-threads (current-processor-count) max-sleep)])
    (displayln threads)

    (map (λ (thread) (join-thread thread #;(+ start-time max-sleep)))
         threads)))

;; See: https://www.gnu.org/software/guile/manual/guile.html#Random
;; Initializing with randomness from platform.
(set! *random-state* (random-state-from-platform))
(main)
